<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/daemon-tag.tld" prefix="d" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<meta http-equiv="expires" content="0"> 
		<meta http-equiv="pragma" content="no-cache"> 
		<meta http-equiv="cache-control" content="no-cache, must-revalidate">
		<link rel="stylesheet" type="text/css" href="style/pagination.css" />
		<link rel="stylesheet" type="text/css" href="style/classic.css" />
		<link rel="stylesheet" type="text/css" href="style/dev.css" />
		<link rel="icon" type="image/png" href="images/nebula20.png" />
	
		<title>Nebula</title>	
		<script language="javascript" src="js/library/jquery-1.8.2.js"></script>
		<script language="javascript" src="js/function.js"></script>
		<script language="javascript" src="js/form.js"></script>
	</head>
	<body>
			<jsp:include page="/common/header.jsp" />		
			<div id="root">	
				<div id="confirmMessage">
					${confirmMessage}
				</div>
				<div id="errorMessage">					
					<c:forEach var="error" items="${errors}">
						${error}<br/>
					</c:forEach>
				</div>				
				<div id="content">								
						
					<jsp:include page="${content}" />						
					
				</div>											
			</div>
			<jsp:include page="/common/footer.jsp" />
	</body>
</html>