<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/daemon-tag.tld" prefix="d" %>

<fmt:setBundle basename="message"/>

<br/><br/>

	<div id="connect" class="myForm">
	
		<h6><fmt:message key="index.log_in"/></h6>
		<form id="connectForm"  method="POST" action="${link700}">
			<p><span><fmt:message key="index.login"/>:</span><input name="login" type="text" /></p>
			<p><span><fmt:message key="index.password"/>:</span><input name="password" type="password" /></p>			
			<p>
				<span>&nbsp;</span><input class="button" type="submit" value="<fmt:message key="index.connect_me"/>"/>
			</p>
			<p>
				<span id="lostPasswordLink">
					<a href="#?w=500" rel="popup_name" class="open_modal"><fmt:message key="index.lost_password"/></a>					
				</span>				
			</p>		
		</form>
	</div>
	<br/>
	<center>
		<img src="images/nebula128.png" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<img src="images/nebula128.png" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<img src="images/nebula128.png" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<img src="images/nebula128.png" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<img src="images/nebula128.png" />
	</center>
	<br/>	
	<div id="register" class="myForm">
		<h6><fmt:message key="index.register"/></h6>
		<form id="registerForm" method="POST" action="${link701}">
			<p><span><fmt:message key="index.lastname"/>:</span><input name="name" type="text" value="${name}" /></p>
			<p><span><fmt:message key="index.firstname"/>:</span><input name="firstname" type="text" value="${firstname}" /></p>
			<p><span><fmt:message key="index.gender"/>:</span><select name="gender"><option></option><option value="M">Male</option><option value="F">Female</option></select></p>
			<p><span>email:</span><input name="email" type="text" value="${email}" /></p>
			<p><span><fmt:message key="index.login"/>:</span><input name="login" type="text" value="${login}" /></p>
			<p><span><fmt:message key="index.password"/>:</span><input name="password" type="password" /></p>
			<p><span><fmt:message key="index.password"/>:</span><input name="confirm_password" type="password" /></p>
			<p><span>&nbsp;</span><input class="button" type="submit" value="Register me"/></p>
		</form>
	</div>
	<div id="popup_name" class="modal">
		<div id="lostPassword" class="myForm">
			<h6>Lost password</h6>
			<form id="lostPasswordForm"  method="POST" action="${link702}">
				<p><span>login:</span><input name="login" type="text" /></p>
				<p><span>email:</span><input name="email" type="text" /></p>			
				<p><span>&nbsp;</span><input class="button" type="submit" value="Send"/></p>
						
			</form>
		</div>
	</div>
