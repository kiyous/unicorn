package net.daemonyum.unicorn.live.utils;

public final class Constant {
	
	// ############################################
	// #          CONTEXT CONFIGURATION           #
	// ############################################	
	public static final String  DOMAIN = "http://spaceworld.is-a-geek.org/";
	//public static final String  CONTEXT_PATH = "";	
	public static final String  CONTEXT_PATH = "LiveShop";
	public static final String  CONTEXT_PATH_SLASH = "LiveShop/";
	public static final String  ABSOLUTE_FILE_PATH = "E:/Programmation/JAVA/rainbow/LiveShop/src/main/webapp";
		
	// ############################################
	// #            URL CONFIGURATION             #
	// ############################################
	//public static final String SERVLET_PACKAGE_NAME = "net.daemonyum.nebula.servlet";
	public static final String WEB_FILE_EXTENSION = "do";	
	
	// ############################################
	// #                JSP PATH                  #
	// ############################################
	public static final String JSP_TEMPLATE_PATH = "/template/";
	public static final String JSP_CONTENT_PATH = "/content/";
	public static final String JSP_AJAX_PATH = "/ajax/";
	public static final String DEFAULT_LINK_NAME = "content";
	
	// ############################################
	// #           DATABASE CONNECTION            #
	// ############################################
	public static final String DRIVER = "org.postgresql.Driver";
	public static final String URL = "jdbc:postgresql://localhost:5432/";
	public static final String DATABASE = "nebula";
	public static final String USER = "postgres";
	public static final String PASSWORD = "reptile59!";
	public static final String SCHEMA = "public";
	
	// ############################################
	// #            MAIL CONFIGURATION            #
	// ############################################
	public static final String SMTP_HOST = "smtp.orange.fr";
	public static final String EMAIL_ADDRESS_FROM = "noreply@orange.fr";
	public static final String PERSONAL_NAME_FROM = "RAINBOW ADMIN";
	public static final String SUBJECT_CREATED = "Rainbow web account created";	
	public static final String SUBJECT_ACTIVATED =  "Rainbow web account activated";	
	
	// ############################################
	// #             COMMON CONSTANT             #
	// ############################################
	public static final String DATEFORMAT = "dd/MM/yyyy HH:mm";
	public static final String PASSWORD_PATTERN = "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%!?]).{6,20})";
	public static final String MOVIE_DRIVE = "JABBA";

}
