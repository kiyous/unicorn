package net.daemonyum.unicorn.live.servlet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.daemonyum.manticora.framework.web.service.servlet.manager.DispatcherServlet;

import org.apache.log4j.Logger;

@SuppressWarnings("serial")
public class MainServlet extends DispatcherServlet {
	
	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(MainServlet.class);

	@Override
	public void monitoring(HttpServletRequest request, HttpServletResponse response) {
		String customerAddress = request.getRemoteAddr();
		LOGGER.debug("@: "+customerAddress);
	}	
	
}
