package net.daemonyum.unicorn.live.filter;

import net.daemonyum.manticora.framework.web.service.servlet.ManticoraServlet;
import net.daemonyum.unicorn.live.servlet.front.IndexServlet;
import net.daemonyum.unicorn.live.servlet.front.ItemServlet;
import net.daemonyum.unicorn.live.servlet.front.ListServlet;

public enum ServletConfigurator implements net.daemonyum.banshee.framework.web.configurator.ServletConfigurator {	
	
		// #########################
		// #        FRONT PERSO    #
		// #########################
//		WELCOME(WelcomeServlet.class,"welcome","link0",true),
		INDEX(IndexServlet.class,"index","link01",true),	
		LIST(ListServlet.class,"list","link02",true),
		ITEM(ItemServlet.class,"item","link03",true),
//		
		;
	
	private Class<? extends ManticoraServlet> servlet;
	private String urlWithoutExtension;
	private final String applicationLink;
	private boolean hasJsp;
	
	
	private ServletConfigurator(Class<? extends ManticoraServlet> servlet,String urlWithoutExtension,String applicationLink){
		this(servlet,urlWithoutExtension,applicationLink,false);
	}
	
	private ServletConfigurator(Class<? extends ManticoraServlet> servlet,String urlWithoutExtension,String applicationLink,boolean hasJsp){
		this.urlWithoutExtension = urlWithoutExtension;
		this.servlet = servlet;
		this.applicationLink = applicationLink;
		this.hasJsp = hasJsp;
	}
	
	public Class<? extends ManticoraServlet> getServlet() {
		return servlet;
	}
	
	public String getUrlWithoutExtension() {
		return urlWithoutExtension;
	}
	
	public final String getApplicationLink() {
		return applicationLink;
	}
	
	public boolean hasJsp() {
		return hasJsp;
	}
	
}
	
	