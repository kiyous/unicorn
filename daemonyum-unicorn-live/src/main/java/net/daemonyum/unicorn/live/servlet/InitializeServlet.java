package net.daemonyum.unicorn.live.servlet;

import net.daemonyum.banshee.framework.web.configurator.Configurator;
import net.daemonyum.banshee.framework.web.service.InitializeProjectServlet;
import net.daemonyum.unicorn.live.filter.JSPConfigurator;
import net.daemonyum.unicorn.live.filter.MobileConfigurator;
import net.daemonyum.unicorn.live.filter.RedirectConfigurator;
import net.daemonyum.unicorn.live.filter.ServletConfigurator;
import net.daemonyum.unicorn.live.utils.Constant;

import org.apache.log4j.Logger;

@SuppressWarnings("serial")
public class InitializeServlet extends InitializeProjectServlet {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(InitializeServlet.class);

	public InitializeServlet() {		
		super(new Configurator(ServletConfigurator.values(),JSPConfigurator.values(),
				RedirectConfigurator.values(), MobileConfigurator.values(), null), Constant.WEB_FILE_EXTENSION);
	}
	
	@Override
	protected void initializeContext() {
		LOGGER.info("####################################");
		LOGGER.info("#### INITIALIZE LIVE CONTEXT  #####");
		LOGGER.info("####################################");
		
	}

}
