package net.daemonyum.unicorn.live.filter;

import net.daemonyum.banshee.framework.web.bean.FormBean;

public enum RedirectConfigurator implements net.daemonyum.banshee.framework.web.configurator.RedirectConfigurator {


	;

	private ServletConfigurator onErrorServlet;
	private ServletConfigurator onSuccessServlet;
	Class<? extends FormBean> form;

	private RedirectConfigurator(ServletConfigurator redirectServlet){
		this.onSuccessServlet = redirectServlet;
		this.onErrorServlet = redirectServlet;
	}

	private RedirectConfigurator(ServletConfigurator onSuccessServlet,ServletConfigurator onErrorServlet){
		this.onSuccessServlet = onSuccessServlet;
		this.onErrorServlet = onErrorServlet;
	}

	@Override
	public void changeErrorServlet(
			net.daemonyum.banshee.framework.web.configurator.ServletConfigurator servletConfigurator) {
		// TODO Auto-generated method stub

	}

	@Override
	public void changeSuccessServlet(
			net.daemonyum.banshee.framework.web.configurator.ServletConfigurator servletConfigurator) {
		// TODO Auto-generated method stub

	}

	@Override
	public ServletConfigurator getErrorServlet() {
		return onErrorServlet;
	}

	@Override
	public Class<? extends FormBean> getForm() {
		return form;
	}

	@Override
	public ServletConfigurator getSuccessServlet() {
		return onSuccessServlet;
	}

}
