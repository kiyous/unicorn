package net.daemonyum.unicorn.live.filter;

import net.daemonyum.unicorn.live.utils.Constant;
import net.daemonyum.unicorn.live.utils.Template;

public enum JSPConfigurator implements net.daemonyum.banshee.framework.web.configurator.JSPConfigurator {
	// la valeur de l'enum doit �tre identique
	//� celle de ServletConfigurator
	WELCOME("/pages/jsp/welcome/","welcome.jsp"),
	INDEX(Template.INDEX_PAGE,"index.jsp",true),
	LIST(Template.INDEX_PAGE,"list.jsp",true),
	ITEM(Template.INDEX_PAGE,"item.jsp",true),
	
	
	;
	
	
	private String templatePath;
	private String jspPath;
	private String linkBetweenTemplateAndContent;
	private boolean mobileVersion;
	
	private JSPConfigurator(Template template, String jspPath) {
		this(template,jspPath,false);
	}
	
	private JSPConfigurator(Template template, String jspPath, boolean mobileVersion) {
		this.templatePath = Constant.JSP_TEMPLATE_PATH+template.getName();
		this.jspPath = Constant.JSP_CONTENT_PATH+jspPath;
		this.linkBetweenTemplateAndContent = Constant.DEFAULT_LINK_NAME;
		this.mobileVersion = mobileVersion;
	}	
	
	private JSPConfigurator(Template template, String jspPath, String linkName) {
		this.templatePath = Constant.JSP_TEMPLATE_PATH+template.getName();
		this.jspPath = Constant.JSP_CONTENT_PATH+jspPath;
		this.linkBetweenTemplateAndContent = linkName;
	}	
	
	private JSPConfigurator(String specialPath,String jspPath) {
		this.templatePath = specialPath+jspPath;
		this.linkBetweenTemplateAndContent = Constant.DEFAULT_LINK_NAME;
	}
	
	/*
	 * Essentiellement pour les pages en retour d'appel AJAX
	 */
	private JSPConfigurator(String jspAjaxPath) {
		this.templatePath = Constant.JSP_AJAX_PATH+jspAjaxPath;
		this.linkBetweenTemplateAndContent = Constant.DEFAULT_LINK_NAME;
	}
	
	public String getContent() {
		return jspPath;
	}
	
	public String getLinkBetweenTemplateAndContent() {
		return linkBetweenTemplateAndContent;
	}
	
	public String getTemplate() {
		return templatePath;
	}
		
	public boolean hasMobileVersion() {
		return mobileVersion;
	}

}
