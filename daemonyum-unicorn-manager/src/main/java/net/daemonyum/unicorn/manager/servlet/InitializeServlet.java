package net.daemonyum.unicorn.manager.servlet;

import net.daemonyum.banshee.framework.web.configurator.Configurator;
import net.daemonyum.banshee.framework.web.service.InitializeProjectServlet;
import net.daemonyum.unicorn.manager.filter.JSPConfigurator;
import net.daemonyum.unicorn.manager.filter.MobileConfigurator;
import net.daemonyum.unicorn.manager.filter.RedirectConfigurator;
import net.daemonyum.unicorn.manager.filter.ServletConfigurator;
import net.daemonyum.unicorn.manager.utils.Constant;

import org.apache.log4j.Logger;

@SuppressWarnings("serial")
public class InitializeServlet extends InitializeProjectServlet {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(InitializeServlet.class);

	public InitializeServlet() {		
		super(new Configurator(ServletConfigurator.values(),JSPConfigurator.values(),
				RedirectConfigurator.values(), MobileConfigurator.values(), null), Constant.WEB_FILE_EXTENSION);
	}
	
	@Override
	protected void initializeContext() {
		LOGGER.info("####################################");
		LOGGER.info("### INITIALIZE MANAGER CONTEXT  ####");
		LOGGER.info("####################################");
		
	}
}
