package net.daemonyum.unicorn.manager.utils;

public enum Template {

	INDEX_PAGE("template0.jsp"),
	
	;
	
	private String name;
	
	private Template(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
