package net.daemonyum.unicorn.manager.filter;

import net.daemonyum.banshee.framework.web.configurator.JSPConfigurator;
import net.daemonyum.unicorn.manager.utils.Constant;
import net.daemonyum.unicorn.manager.utils.Template;

public enum MobileConfigurator implements JSPConfigurator{

	
	
	;
	
	private final String defaultLinkName = "content";
	private String templatePath;
	private String jspPath;
	private String linkBetweenTemplateAndContent;
	
	private MobileConfigurator(Template template, String jspPath) {
		this.templatePath = Constant.JSP_TEMPLATE_PATH+template.getName();
		this.jspPath = Constant.JSP_CONTENT_PATH+jspPath;
		this.linkBetweenTemplateAndContent = defaultLinkName;
	}

	public String getContent() {
		return jspPath;
	}

	public String getLinkBetweenTemplateAndContent() {
		return linkBetweenTemplateAndContent;
	}

	public String getTemplate() {
		return templatePath;
	}
	
	public boolean hasMobileVersion() {
		return false;
	}

}
